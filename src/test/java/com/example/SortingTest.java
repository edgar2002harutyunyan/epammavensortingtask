package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.*;

public class SortingTest {

    @Test
    public void testZeroElementCase() {
        int[] result = {};
        int[] expected = {};
        Sorting.insertionSort(result);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testOneElementCase(){
        int[] result = {73};
        int[] expected = {73};
        Sorting.insertionSort(result);
        assertArrayEquals(expected, result);
    }

    @Test
    public void testTenElementCase(){
        int[] result = {6, 0, 1, 0, 12,22, 1, 3, 8, 20};
        int[] expected = {0, 0, 1, 1, 3, 6, 8, 12, 20, 22};
        Sorting.insertionSort(result);
        assertArrayEquals(expected, result);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testMoreThanTenElementsCase(){
        int[] result = {6, 0, 423, 45, 1, 0, 12, 22, 1, 54, 3, 8, 20};
        Sorting.insertionSort(result);
    }

}
