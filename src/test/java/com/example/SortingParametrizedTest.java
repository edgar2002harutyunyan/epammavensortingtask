package com.example;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingParametrizedTest {
    int[] arr;
    int[] expected;

    public SortingParametrizedTest(int[] arr, int[] expected){
        this.arr = arr;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]>data(){
        return Arrays.asList(new Object[][]{
                {new int[] {3, 1, 5, 6, 10},new int[]{1,3,5,6,10}},
                {new int[] {5, 32, 18, 92, 6, 0, 54, 73, 100, 41}, new int[] {0, 5, 6, 18, 32, 41, 54, 73, 92, 100}},
                {new int[]{100,99,98,97,96}, new int[]{96,97,98,99,100}},
                {new int[]{1,1,1,1,1,1}, new int[] {1,1,1,1,1,1}}
        });

    }

    @Test
    public void testParametrizedTests(){
        Sorting.insertionSort(arr);
        assertArrayEquals(expected, arr);
    }
}
