package com.example;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class Sorting {
    public static void main(String[]args){
        ArrayList<Integer> tempArr= new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        int i = 0;
        while(sc.hasNextInt() && i < 10){
            tempArr.add(sc.nextInt());
            i++;
        }
        int[] array = new int[tempArr.size()];
        copyFromArrayList(tempArr, array);
        insertionSort(array);
        printArr(array);
    }

    public static void copyFromArrayList(ArrayList arrayList, int[]array){
        for(int i = 0; i<arrayList.size(); i++){
            array[i] = (int) arrayList.get(i);
        }
    }

    public static void printArr(int[] array){
        for (int j: array
        ) {
            System.out.println(j);
        }
    }


    public static void insertionSort(int[] arr) {
        if(arr.length > 10){
            throw new IllegalArgumentException();
        }
        int length = arr.length;
        for (int j = 1; j < length; j++) {
            int key = arr[j];
            int i = j - 1;
            while ((i > -1) && (arr[i] > key)) {
                arr[i + 1] = arr[i];
                i--;
            }
            arr[i + 1] = key;
        }
    }


}
